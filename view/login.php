<?php
include "../template/header.html";
include "../fileType.php";
?>
    <link rel="stylesheet" href="../static/user/css/global.css">
    <link rel="stylesheet" href="../static/user/css/signup.css">

    <body class="bg-light">
    <div class="container " id="mainContent">
        <h1 class="d-flex justify-content-center">Login with Password</h1>
        <div class="col-5 mx-auto">
            <form id="loginForm">
                <div class="form-group">
                    <label for="emailField">Email address</label>
                    <input type="email" class="form-control" id="emailField" name="emailField"
                           aria-describedby="emailHelp"
                           placeholder="Enter email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                    </small>
                </div>
                <div class="form-group">
                    <label for="passwordField">Password</label>
                    <input type="password" class="form-control" id="passwordField" name="passwordField"
                           placeholder="Password">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="loginCheckBox">
                    <label class="form-check-label" for="loginCheckBox">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    </body>
<?php include "../template/footer.html"; ?>