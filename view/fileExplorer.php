<?php
if (!isset($_SESSION['role']) || $_SESSION['role'] == "Error") {
    header("Location: http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/' . basename(dirname(dirname(__FILE__))) . '/view/login.php');
}
include "template/header.html";
include "fileType.php";
include "controllers/fileExplorerController.php";
$FEController = new fileExplorerController();
define('CONFIG_URL', $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/' . basename(dirname(dirname(__FILE__))));
?>
<div class="container">
    <a href="#" class="btn btn-warning btn-lg float-right" id="logoutUser">
        <span class="glyphicon glyphicon-user">Logout</span>
    </a>
    <a href="#" class="btn btn-info btn-lg float-right ">
        <span class="glyphicon glyphicon-user" id="loginedUser"><?php echo $_SESSION['Account']; ?></span>
    </a>
</div>
<div id="breadCrumbNav" class="container">
    <ol class="breadcrumb">
        <?php
        $currentPath = $FEController->getCurrentDir();
        $currentPath = $FEController->getRelativePath($currentPath);
        $tmpDirArray = explode('/', $currentPath);
        $remenu = '';
        foreach ($tmpDirArray as $menu) {
            $remenu = $remenu . "/" . $menu;
            if ($remenu == "/") {
                $menu = "SimpleExplorer";
            }
            ?>
        <li class='breadcrumb-item'><a href="index.php?dir=<?php echo $FEController->trimSlash($remenu) ?> ">
                <?php echo $menu ?></a>
        </li>
        <?php } ?>
    </ol>
</div>
<div id="manipulationOpts" class="container list-inline">
    <ol>
        <li class="list-inline-item"><a class="btn btn-primary" id="folderBtn" href="#" onclick=showCreateInputBox();
                role="button">new folder</a></li>
        <li class="list-inline-item"><a class="btn btn-danger" id="deleteBtn" href="#"
                onclick=showDeleteInputBox();>delete</a>
        </li>
        <li class="list-inline-item"><a class="btn btn-primary" id="copyBtn" href="#"
                onclick=showCopyInputBox();>copy</a></li>
        <li class="list-inline-item"><a class="btn btn-primary" id="pasteBtn" href="#"
                onclick=showPasteInputBox();>paste</a>
        </li>
        <li class="list-inline-item"><a class="btn btn-primary" href="#" onclick=showModifyInputBox();>modify
                name</a></li>
        <li class="list-inline-item float-right d-none" id="actionLi">
            <div class="input-group createFolderGP">
                <input class="form-control" type="text" name="defaultInputBox" id="defaultInputBox">
                <span class="input-group-btn">
                    <a class="btn btn-primary" role="button" id="defaultID" href="#" btnClicked="default"
                        onclick=actionSwitch('<?php echo CONFIG_URL; ?>','<?php echo $FEController->getCurrentDir(); ?>');>
                        Confirm</a></span>
            </div>
        </li>
    </ol>
</div>
<div id="content" class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">File name</th>
                <th scope="col">created time</th>
                <th scope="col">file size</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            foreach ($FEController->getCurrentDirFiles() as $file) {
                $icon = "";
                $size = 0;
                $fullPath = $FEController->getCurrentDir() . "/" . $file;
                $createdTime = $FEController->getCreatedTime($fullPath);
                $url = '';
                if (in_array($file, $FEController->getIgnoreDir(), true) || strpos($file, '.php') || strpos($file, '.') === 0) {
                    continue;
                }
                if (is_dir($fullPath)) {
                    if ($file == '..') {
                        $url = $FEController->getUpDirUrl($fullPath);
                        $size = "n.a.";
                    } else {
                        $url = $FEController->getDirUrl($fullPath);
                        $size = $FEController->formatFileSize($FEController->getfolderSize($fullPath));
                    }
                    $icon = "fas fa-folder";
                } else {
                    $url = $FEController->getFileURL($fullPath);
                    $icon = $FEController->getFileExtension($fullPath);
                    $size = $FEController->formatFileSize(filesize($fullPath));
                }
                $i++;

                ?>
            <tr id="id<?php echo $i ?>">
                <td>
                    <div class="wrapper form-check">
                        <input class="form-check-input" type="radio" value="" name="radioBtnGp"
                            id="radio<?php echo $i ?>">
                        <?php if (is_dir($fullPath)) { ?>
                        <a href="<?php echo $url; ?>" id="url_<?php echo $i; ?> "
                            pathDetails=<?php echo $FEController->getCurrentDir() . '/' . $file; ?>> <i
                                class="<?php echo $icon; ?>"></i>
                            <?php echo $file; ?></a>
                        <?php } else { ?>
                        <a href="#" pathDetails=<?php echo $FEController->getCurrentDir() . '/' . $file; ?>
                            onclick=layerOpenURL("<?php echo "http://" . CONFIG_URL . '/' . substr($FEController->trimSlash($url), 2); ?>");
                            id="url_<?php echo $i; ?> "> <i class="<?php echo $icon; ?>"></i> <?php echo $file; ?></a>
                        <?php } ?>
                    </div>
                </td>
                <td><?php echo $createdTime ?></td>
                <td><?php echo $size ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>
let locationFolderName = window.location.pathname.substr(0, window.location.pathname.indexOf('/',
    1));
document.getElementById("logoutUser").href = "http://" + window.location.host + locationFolderName +
    "/view/logout.php";
</script>

<?php include "template/footer.html"; ?>