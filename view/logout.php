<?php
session_start();
define('LOGIN_URL', "http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/' . basename(dirname(dirname(__FILE__))) . '/view/login.php');
session_destroy();
header("Location:". LOGIN_URL);
exit();
