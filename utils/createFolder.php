<?php
session_start();
if (!isset($_SESSION['role']) || $_SESSION['role'] !== 'admin') {
    die("no priviledge");
}
$action = $_GET["action"];
$filePath = $_GET["path"];
function recurse_copy($src, $dst)
{
    $dir = opendir($src);
    $dst = $dst . '/' . basename($src);
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                recurse_copy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function rmdir_recursive($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . "/" . $object))
                    rmdir_recursive($dir . "/" . $object);
                else
                    unlink($dir . "/" . $object);
            }
        }
        rmdir($dir);
    }
}

function chmod_r($path)
{
    $dir = new DirectoryIterator($path);
    foreach ($dir as $item) {
        chmod($item->getPathname(), 0777);
        if ($item->isDir() && !$item->isDot()) {
            chmod_r($item->getPathname());
        }
    }
}

switch ($action) {
    case "createFolder":
        if (!mkdir($filePath, 0, true)) {
            die("failed");
        } else {
            chmod($filePath, 0777);
        }
        break;
    case "delete":
        if (is_file($filePath)) {
            unlink($filePath);
        } else if (is_dir($filePath)) {
            rmdir_recursive($filePath);
        } else {
            die("unknown type");
        }
        break;
    case "paste":
        $srcPath = @$_GET["srcPath"];
        if (file_exists($srcPath)) {
            if (is_dir($srcPath)) {
                if ($srcPath != "") {
                    recurse_copy($srcPath, $filePath);
                }
            } else {
                copy($srcPath, $filePath . '/' . basename($srcPath));
            }
        }
        chmod_r($filePath);
        break;
    case "modify":
        $newName = @$_GET["newName"];
        $oldName = pathinfo($filePath, PATHINFO_FILENAME);
        $extensionName = pathinfo($filePath, PATHINFO_EXTENSION);
        if (is_dir($filePath)) {
            $newName = dirname($filePath) . '/' . $newName;
            if (realpath($newName) === realpath(pathinfo($filePath, PATHINFO_DIRNAME))) {
                return;
            }
            rename($filePath, $newName);
        } else {
            if (is_dir($newName)) {
                $newName = $oldName;
            }
            $newName = dirname($filePath) . '/' . $newName . '.' . $extensionName;
            rename($filePath, $newName);
        }
        break;
    default:
        break;
}
