<?php
session_start();

class loginValidate
{
    function formValidate()
    {
        $emailField = @$_POST['emailField'];
        $passwordField = @$_POST['passwordField'];
        if (!isset($emailField) || empty($emailField) || !isset($passwordField) || empty($passwordField)) {
            die("error data");
        } else {
            if ($emailField == "admin@gmail.com" && sha1($passwordField) == 'd033e22ae348aeb5660fc2140aec35850c4da997') {
                $return['Account'] = "admin";
                $return['role'] = 'admin';
                $_SESSION['Account'] = "admin@gmail.com";
                $_SESSION['role'] = 'admin';
//                $location='Location: '.$_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/' . basename(dirname(dirname(__FILE__)));
//                header($location);
                return json_encode($return);
            } else if ($emailField == 'user@gmail.com' && sha1($passwordField) == '12dea96fec20593566ab75692c9949596833adc9') {
                $return['Account'] = "user";
                $return['role'] = 'user';
                $_SESSION['Account'] = "user@gmail.com";
                $_SESSION['role'] = 'user';
                return json_encode($return);
            } else {
                $return['Account'] = 'Error';
                return json_encode($return);
            }
        }
    }
}

$instance = new loginValidate();
echo $instance->formValidate();