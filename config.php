<?php
define('BASIC_PATH', rtrim(dirname(__FILE__), '/') . '/');
define('STATICPATH', BASIC_PATH . 'static/');
define('CONTROLLERPATH', BASIC_PATH . 'controllers/');
define('TEMPLATE', BASIC_PATH . 'template/');
define('DATA_PATH', BASIC_PATH . 'data/');
define('USER_PATH', DATA_PATH . 'user/');
define('COMMONLIBRARY', STATICPATH . 'library');
define('USERLIBRARY', STATICPATH . 'user');
define('VIEWPATH', BASIC_PATH . 'view/');

include("fileType.php");
include(CONTROLLERPATH . 'applicationController.php');
