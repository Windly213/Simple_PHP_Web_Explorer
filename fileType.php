<?php
$typeArray = array(
    "dir" => "fas fa-folder",
    "md" => "fab fa-markdown",
    "txt" => "fas fa-file-word",
    "default" => "fas fa-file-alt",
    "pdf"=>"far fa-file-pdf"
);