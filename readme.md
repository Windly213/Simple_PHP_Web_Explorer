# Readme and User Manual



## Setup (***Important!***)

- #### **Path restriction**: All files must be placed under a folder which is directly under the `Apache` or `Nginx` webroot, for example. `D:\WAMP\www\explorer` , where the webroot is `D:\WAMP\www\` . And all the files are placed under that `explorer` folder.

- login Email address and password
  - for admin privilege: 
    - email: `admin@gmail.com`
    - password: `admin`
  - for normal user privilege:
    - email: `user@gmail.com`
    - password: `user`



## Screen capture and function introduction

1. login page![](assets/Capture.PNG)
2. login using the `admin@gmail.com`, then it shows:![](assets/Capture2.PNG)



### Create Folder function

1. Click the `new folder` button, the input box and confirm button appears. Click `Create Folder `, then the folder is created and the page is automatically refreshed.![](assets/Capture3.PNG)
2. Default sorting rule: Folder first, file second. Capital letters first, small letters second.![](assets/capture4.PNG)



### Delete function

1.  Select the folder or file using the radio button. Then click delete button. ![](assets/capture5-1568615404918.PNG)
2. Click on the `confirm delete` button, the file/ folder will be removed.![](assets/capture6.PNG)





### copy and paste function

1. select the folder or file with radio button. Then click the `copy` button.
2. go to any of the folder. click `paste` button, and click the `confirm paste` button on the right hand side. eg: I copied the file on `/` to `/view` ![](assets/Capture7.PNG)



### modify function

1. select one file or folder using radio button, click the `modify name` button.
2. input the name to be modified. (modifying name will not change the extension name)
3. eg. I changed the pasted pdf from `MahtmeticsPaper.pdf` to `Thesis.pdf` . ![](assets/capture8.PNG)

### Exisitng feature

layer file preview using layer library

![](assets/2.PNG)

### user control

1. click logout return to the login page.
2. login using the `user@gmail.com` account.![](assets/capture9.PNG)
3. All the file manipulation function will be disabled by backend, including creation,deletion,pasting and modification. User can only view the current folders.



### Security Issue

1. Prevent unauthorized access by posting directly to the PHP APIs. Eg, the user directly post a `Get` call to the createFolder API. The PHP script only accept the request as long as the admin session is alive. otherwise no action will be taken:  ![](assets/capture10.PNG)
2. default hiding all the PHP files and files starts with `.` such `.git` for higher security
3. Intercept any kinds of penetration visit to other directories, like using the `/..` to visiting the upper directory outside `explorer` folder. Automatically redirect to the `/` ![](assets/capture11.PNG)
4. the password is predefined and encrypted using Sha1 algorithm. such as `sha1($passwordField) == 'd033e22ae348aeb5660fc2140aec35850c4da997')` which makes the password verification process more secure.
