<?php

class applicationController
{
    private $DefaultController = null;
    private $DefaultAction = null;

    public function setDefaultController($controller)
    {
        $this->DefaultController = $controller;
    }

    public function setDefaultAction($DefaultAction)
    {
        $this->DefaultAction = $DefaultAction;
    }


    public function runControllerAndMethod($controllerClass, $method)
    {
        $filePath = CONTROLLERPATH . $controllerClass . '.php';
        if (is_file($filePath)) {
            require_once $filePath;
            if (class_exists($controllerClass)) {
                $instance = new $controllerClass();
                if (method_exists($instance, $method)) {
                    $instance->$method();
                } else {
                    die("No such method");
                }
            } else {
                die("No such class");
            }
        } else {
            die("No such file");
        }
    }

    public function autoRun()
    {
        $runVerification = array("verificationController" => "login");
        foreach ($runVerification as $controller => $method) {
            $this->runControllerAndMethod($controller, $method);
        }
    }

    public function apprun()
    {
        if (isset($this->DefaultController) && isset($this->DefaultAction)) {
            $viewFile = VIEWPATH . 'fileExplorer.php';
            require_once $viewFile;

        }
    }
}
