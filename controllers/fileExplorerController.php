<?php

class fileExplorerController
{

    private $dirFiles;
    protected $currentDir = '';
    private $ignoreDir = array(".", ".git");


    public function __construct()
    {
        $dir = @$_GET['dir'];
        if ($this->startsWith($dir, "..")) {
            exit("error");
        } else if ($dir === '/' || $dir == ""||$this->startsWith($dir,"/..") || $this->startsWith($dir, './')) {
            $dir = "/";
        }
        $trimmedPath = $this->trimSlash(dirname(dirname(__FILE__)) . $dir);
        if ($this->isValidDir($trimmedPath)) {
            $this->setCurrentDir($trimmedPath);
            $this->dirFiles = scandir($this->getCurrentDir());
            $this->sortDirFiles();
        }
    }

    private function startsWith($string, $needle)
    {
        return (substr($string, 0, strlen($needle)) === $needle);
    }

    public function endsWith($string, $endString)
    {
        $len = strlen($endString);
        if ($len == 0) {
            return true;
        }
        return (substr($string, -$len) === $endString);
    }

    public function trimSlash($path)
    {
        $path = trim($path);
        $path = str_replace("\\", "/", $path);
        $path = str_replace("//", "/", $path);
        return $path;
    }

    public function isValidDir($path)
    {
        if (isset($path)) {
            if (is_dir($path)) {
                return true;
            } elseif (is_file($path)) {
                return false;
            }
        }
        return false;
    }

    public function getCurrentDir()
    {
        return $this->currentDir;
    }

    public function sortDirFiles()
    {
        $tmpDirArray = array();
        $tmpFileArray = array();
        foreach ($this->dirFiles as $value) {
            $completePath = $this->trimSlash($this->getCurrentDir() . "/" . $value);
            if (!$this->isValidPath($completePath)) {
                echo "wrong path!";
                exit(1);
            } else {
                if (is_dir($completePath)) {
                    array_push($tmpDirArray, $value);
                } else {
                    array_push($tmpFileArray, $value);
                }
            }
        }
        $this->dirFiles = array_merge($tmpDirArray, $tmpFileArray);
    }

    public function isValidPath($path)
    {
        if (isset($path)) {
            if (is_dir($path)) {
                return true;
            } elseif (is_file($path)) {
                return true;
            }
        }
        return false;
    }

    public function getRootPath()
    {
        return $this->trimSlash(dirname(dirname(__FILE__)));
    }

    public function getCurrentDirFiles()
    {
        return $this->dirFiles;
    }

    public function getIgnoreDir()
    {
        return $this->ignoreDir;
    }

    public function getDirUrl($relativepath)
    {
        return "index.php?dir=" . $this->getRelativePath($relativepath);
    }

    public function getRelativePath($absolutePath)
    {
        $prefix = $this->trimSlash(dirname(dirname(__FILE__)));
        if (substr($absolutePath, 0, strlen($prefix)) == $prefix) {
            $absolutePath = substr($absolutePath, strlen($prefix));
        }
        return $this->trimSlash($absolutePath);
    }

    public function getFileURL($absolutePath)
    {
        return $this->trimSlash(dirname(basename(__DIR__)) . $this->getRelativePath($absolutePath));
    }

    public function getiFrameFileURL($absolutePath)
    {
        return $this->getRelativePath($absolutePath);
    }

    public function getUpDirUrl($absolutePath)
    {
        return "index.php?dir=" . $this->upDir($this->getRelativePath($absolutePath));
    }

    public function upDir($dir)
    {
        $upDir = '';
        $tmpDirArray = explode('/', $dir);
        if (count($tmpDirArray) == 3) {
            $upDir = "/";
        } else {
            for ($i = 1; $i < count($tmpDirArray) - 2; $i++) {
                $upDir = $upDir . '/' . $tmpDirArray[$i];
            }
        }
        return $this->trimSlash($upDir);
    }

    public function getfolderSize($path)
    {
        $bytestotal = 0;
        $path = realpath($path);
        if ($path !== false && $path != '' && file_exists($path)) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {
                try {
                    $bytestotal += filesize($object);
                } catch (\http\Exception\RuntimeException $e) {
                }
            }
        }
        return $bytestotal;
    }


    public function getCreatedTime($fullPath)
    {
        $createdTime = date("Y-m-d H:i", filectime($fullPath));
        return $createdTime;
    }

    public function getFileExtension($fullPath)
    {
        $typeArray = array(
            "dir" => "fas fa-folder",
            "md" => "fab fa-markdown",
            "txt" => "fas fa-file-word",
            "default" => "fas fa-file-alt",
            "pdf" => "far fa-file-pdf",
        );
        $extensionArray = explode(".", $fullPath);
        $extension = end($extensionArray);
        return array_key_exists($extension, $typeArray) ? $typeArray[$extension] : "fas fa-file-alt";
    }

    public function formatFileSize($fileSize)
    {
        if ($fileSize >= 1073741824) {
            $fileSize = number_format($fileSize / 1073741824, 2) . ' GB';
        } elseif ($fileSize >= 1048576) {
            $fileSize = number_format($fileSize / 1048576, 2) . ' MB';
        } elseif ($fileSize >= 1024) {
            $fileSize = number_format($fileSize / 1024, 2) . ' KB';
        } elseif ($fileSize > 1) {
            $fileSize = $fileSize . ' bytes';
        } elseif ($fileSize == 1) {
            $fileSize = $fileSize . ' byte';
        } else {
            $fileSize = '0 byte';
        }

        return $fileSize;
    }

    public function setCurrentDir($dir)
    {
        $this->currentDir = $dir;
    }

    public function breadcrumbCode()
    {
        $currentPath = $this->getCurrentDir();
        $currentPath = $this->getRelativePath($currentPath);
        $tmpDirArray = explode('/', $currentPath);
        $remenu = '';
        $breadcrumbHTML = "";
        foreach ($tmpDirArray as $menu) {
            $remenu = $remenu . "/" . $menu;
            if ($remenu == "/") {
                $menu = "SimpleExplorer";
            }
            $liNode = '<li class="breadcrumb-item"><a href="index.php?dir=' . $this->trimSlash($remenu) . '">' . $menu . '</a></li>';
            $breadcrumbHTML .= $liNode;
        }
    }
}
